package davidmontaguano.tiendacocinas.base;

import java.time.LocalDate;

public class CocinaAmericana extends TiendaCocinas{
    private String tamanioBarra;

    //Constructor para que sea visible y pueda ser leido por otras clases.
    public CocinaAmericana(){
        super();
    }

    public CocinaAmericana(String nombreAccesorio, String marca, String movilidad, int medida, LocalDate fechaCompra, String tamanioBarra){
        super(nombreAccesorio,marca,movilidad,medida,fechaCompra);
        this.tamanioBarra=tamanioBarra;
    }

    public String getTamanioBarra() {
        return tamanioBarra;
    }

    public void setTamanioBarra(String tamanioBarra) {
        this.tamanioBarra = tamanioBarra;
    }

    @Override
    public String toString() {
        return "Informacion Cocina Americana:     "+
                "Nombre Accesorio: " + getNombreAccesorio()+"    "+
                "Marca: "+ getMarca()+"    "+
                "Movilidad: "+getMovilidad()+"    "+
                "Medida: "+getMedida()+"    "+
                "Fecha de Compra: "+getFechaCompra()+"    "+
                "Tamaño de la Barra: "+getTamanioBarra();
    }
}
