package davidmontaguano.tiendacocinas.base;

import java.time.LocalDate;

public abstract class TiendaCocinas {

    private String nombreAccesorio;
    private String marca;
    private String movilidad;
    private int medida;
    private LocalDate fechaCompra;


    public TiendaCocinas(String nombreAccesorio, String marca, String movilidad, int medida, LocalDate fechaCompra){

        this.nombreAccesorio=nombreAccesorio;
        this.marca=marca;
        this.movilidad=movilidad;
        this.medida=medida;
        this.fechaCompra=fechaCompra;
    }

    public TiendaCocinas(){}

    public String getNombreAccesorio() {
        return nombreAccesorio;
    }

    public void setNombreAccesorio(String nombreAccesorio) {
        this.nombreAccesorio = nombreAccesorio;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getMovilidad() {
        return movilidad;
    }

    public void setMovilidad(String movilidad) {
        this.movilidad = movilidad;
    }

    public int getMedida() {
        return medida;
    }

    public void setMedida(int medida) {
        this.medida = medida;
    }

    public LocalDate getFechaCompra() {
        return fechaCompra;
    }

    public void setFechaCompra(LocalDate fechaCompra) {
        this.fechaCompra = fechaCompra;
    }
}
