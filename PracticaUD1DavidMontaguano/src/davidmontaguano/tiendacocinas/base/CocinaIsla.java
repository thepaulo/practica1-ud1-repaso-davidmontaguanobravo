package davidmontaguano.tiendacocinas.base;

import java.time.LocalDate;

public class CocinaIsla extends TiendaCocinas{
    private int nEncimeras;

    //Constructor para que sea leida por otras clases

    public CocinaIsla(){
        super();
    }

    public CocinaIsla(String nombreAccesorio, String marca, String movilidad, int medida, LocalDate fechaCompra, int nEncimeras){
        super(nombreAccesorio,marca,movilidad,medida,fechaCompra);
        this.nEncimeras=nEncimeras;
    }

    public int getnEncimeras() {
        return nEncimeras;
    }

    public void setnEncimeras(int nEncimeras) {
        this.nEncimeras = nEncimeras;
    }

    @Override
    public String toString() {
        return "Informacion Cocina Isla:     "+
                "Nombre Accesorio: " + getNombreAccesorio()+"    "+
                "Marca: "+ getMarca()+"    "+
                "Movilidad: "+getMovilidad()+"    "+
                "Medida: "+getMedida()+"    "+
                "Fecha de Compra: "+getFechaCompra()+"    "+
                "Numero de encimeras: " + getnEncimeras()+" encimeras";
    }
}
