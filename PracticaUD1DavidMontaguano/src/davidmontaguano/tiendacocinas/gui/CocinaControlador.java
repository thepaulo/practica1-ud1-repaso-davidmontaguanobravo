package davidmontaguano.tiendacocinas.gui;

import davidmontaguano.tiendacocinas.base.CocinaAmericana;
import davidmontaguano.tiendacocinas.base.CocinaIsla;
import davidmontaguano.tiendacocinas.base.TiendaCocinas;
import davidmontaguano.tiendacocinas.util.Util;
import org.xml.sax.SAXException;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.xml.crypto.dsig.TransformException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Properties;

public class CocinaControlador implements ActionListener, ListSelectionListener, WindowListener {

    private Ventana vista;
    private CocinaModelo modelo;
    private File uRutaExportada;

    public CocinaControlador(Ventana vista,CocinaModelo modelo){
        this.vista=vista;
        this.modelo=modelo;
        try{
            cargarDatosConfiguracion();
        }catch (IOException e){
            e.printStackTrace();
        }

        addActionListener(this);
        addListSelectionListener(this);
        addWindowListener(this);
    }

    private boolean hayCamposVacios(){
        if(vista.americanaIslaTxt.getText().isEmpty() ||
            vista.accesorioTxt.getText().isEmpty()||
                vista.marcaTxt.getText().isEmpty()||
                vista.movilidadTxt.getText().isEmpty()||
                vista.dimensionTxt.getText().isEmpty()||
                vista.fechaCompraDPicker.getText().isEmpty())
        {
            return true;
        }
        return false;
    }

    private void limpiarCampos(){
        vista.americanaIslaTxt.setText(null);
        vista.accesorioTxt.setText(null);
        vista.marcaTxt.setText(null);
        vista.movilidadTxt.setText(null);
        vista.dimensionTxt.setText(null);
        vista.fechaCompraDPicker.setDate(null);
    }

    public void refrescar(){
        vista.dlmTiendaCocinas.clear();
        for(TiendaCocinas unAccesorio : modelo.obtenerCocinas()){
            vista.dlmTiendaCocinas.addElement(unAccesorio);
        }
    }

    private void addActionListener(ActionListener listener){
        vista.americanaRadioB.addActionListener(listener);
        vista.islaRadioB.addActionListener(listener);
        vista.nuevoBtn.addActionListener(listener);
        vista.exportarBtn.addActionListener(listener);
        vista.importarBtn.addActionListener(listener);
        vista.salirBtn.addActionListener(listener);
        vista.eliminarBtn.addActionListener(listener);
        vista.buscarBtn.addActionListener(listener);
    }

    private void addWindowListener(WindowListener listener){
        vista.frame.addWindowListener(listener);
    }

    private void addListSelectionListener(ListSelectionListener listener){
        vista.list1.addListSelectionListener(listener);
    }

    private void cargarDatosConfiguracion() throws IOException{
        Properties configuracion = new Properties();
        configuracion.load(new FileReader("cocinas.conf"));
        uRutaExportada = new File(configuracion.getProperty("uRutaExportada"));
    }

    private void actualizarDatosConfiguracion(File uRutaExportada){
        this.uRutaExportada=uRutaExportada;
    }

    private void guardarConfiguracion() throws IOException{
        Properties configuracion=new Properties();
        configuracion.setProperty("uRutaExportada",uRutaExportada.getAbsolutePath());
        configuracion.store(new PrintWriter("cocinas.conf"),"Datos configuracion cocinas");
    }

    private void cargar(TiendaCocinas nombre){
        if(nombre==null)
            return;
        vista.accesorioTxt.setText(nombre.getNombreAccesorio());
        vista.marcaTxt.setText(nombre.getMarca());
        vista.movilidadTxt.setText(nombre.getMovilidad());
        vista.dimensionTxt.setText(String.valueOf(nombre.getMedida()));
        vista.fechaCompraDPicker.setDate(nombre.getFechaCompra());
        if(nombre instanceof CocinaAmericana){
            vista.americanaIslaTxt.setText(((CocinaAmericana) nombre).getTamanioBarra());
        }
        else{
            vista.americanaIslaTxt.setText(String.valueOf(((CocinaIsla)nombre).getnEncimeras()));
        }





    }
    /*
    Metodo que usaremos para que los botones funcionen correctamente y cumplan su funcion
     */
    @Override
    public void actionPerformed(ActionEvent e) {

        String actionCommand=e.getActionCommand();
        TiendaCocinas nombre=null;

        switch (actionCommand){

            case "NUEVO":
                if(hayCamposVacios()){
                    Util.mensajeError("Los siguientes campos no pueden estar vacios \nAccesorio \nFecha Compra"+
                            vista.americanaIslaLbl.getText());
                    break;
                }

                if(modelo.existeAccesorio(vista.accesorioTxt.getText())){
                    Util.mensajeError("El accesorio "+vista.accesorioTxt.getText()+" ya existe");
                    break;
                }

                if(vista.americanaRadioB.isSelected()){
                    modelo.altaAmericana(vista.accesorioTxt.getText(),vista.marcaTxt.getText(),vista.movilidadTxt.getText(),
                    Integer.parseInt(vista.dimensionTxt.getText()),vista.fechaCompraDPicker.getDate(),vista.americanaIslaTxt.getText());
                }
                else{
                    modelo.altaIsla(vista.accesorioTxt.getText(),vista.marcaTxt.getText(),vista.movilidadTxt.getText(),
                            Integer.parseInt(vista.dimensionTxt.getText()),vista.fechaCompraDPicker.getDate(),Integer.parseInt(vista.americanaIslaTxt.getText()));

                }

                limpiarCampos();
                refrescar();
                break;

            case "IMPORTAR":

                JFileChooser selectorFichero = Util.crearSelectorFicheros(uRutaExportada, "Archivo XML", "xml");
                int opt = selectorFichero.showOpenDialog(null);
                if(opt ==JFileChooser.APPROVE_OPTION){
                    // El try catch nos sirve para controlar los errores de un bloque de codigo en concreto
                    try{
                        modelo.importarXML(selectorFichero.getSelectedFile());
                    }catch (ParserConfigurationException ex){
                        ex.printStackTrace();
                    }catch (IOException ex){
                        ex.printStackTrace();
                    }catch (SAXException ex){
                        ex.printStackTrace();
                    }
                    refrescar();
                }

                break;

            case "EXPORTAR":

                JFileChooser selectorFichero2 = Util.crearSelectorFicheros(uRutaExportada,"Archivo XML", "xml");
                int opt2=selectorFichero2.showSaveDialog(null);
                if(opt2 == JFileChooser.APPROVE_OPTION){
                    try{
                        modelo.exportarXML(selectorFichero2.getSelectedFile());
                        actualizarDatosConfiguracion(selectorFichero2.getSelectedFile());
                    }catch (ParserConfigurationException ex){
                        ex.printStackTrace();
                    } catch (TransformerException ex) {
                        ex.printStackTrace();
                    }
                }

                break;

            case "SALIR":
                int resp = Util.mensajeConfirmacion("¿Desea salir ?", "Salir");
                if(resp == JOptionPane.OK_OPTION){
                    try{
                        guardarConfiguracion();
                    }catch (IOException e1){
                        e1.printStackTrace();
                    }
                    System.exit(0);
                }

                break;

            case "ELIMINAR":

                if (JOptionPane.showConfirmDialog(null, "¿Está seguro?", "Eliminar",
                        JOptionPane.YES_NO_OPTION) == JOptionPane.NO_OPTION)
                    return;

                modelo.eliminar();
                nombre = modelo.getActual();
                refrescar();

                break;

            case "BUSCAR":

                nombre = modelo.buscar(vista.buscarTxt.getText());
                if (nombre == null) {
                    Util.mensajeError("No se ha encontrado el articulo : "+vista.buscarTxt.getText());
                    return;
                }

                cargar(nombre);
                break;


            case "Cocina Americana":

                vista.americanaIslaLbl.setText("Cocina Americana: Tamaño Barra(pequeña,mediana,grande)");
                break;

            case "Cocina Isla":

                vista.americanaIslaLbl.setText("Cocina Isla: Nº Encimeras");
                break;

        }
    }
    //Metodos para realizar diferentes acciones con la ventana de ejecucion
    @Override
    public void windowOpened(WindowEvent e) {
        //System.out.println("Bienvenido A Nuestra APP");
        Util.mensajeInicio("Bienvenido A Nuestra APP");
    }

    @Override
    public void windowClosing(WindowEvent e) {

        int resp = Util.mensajeConfirmacion("¿Desea salir de la ventana?", "Salir");
        if(resp == JOptionPane.YES_OPTION){
            try{
                guardarConfiguracion();
            }catch (IOException e1){
                e1.printStackTrace();
           }
            System.exit(0);
        }
    }

    @Override
    public void windowClosed(WindowEvent e) {

    }

    @Override
    public void windowIconified(WindowEvent e) {

;    }

    @Override
    public void windowDeiconified(WindowEvent e) {

    }

    @Override
    public void windowActivated(WindowEvent e) {

    }

    @Override
    public void windowDeactivated(WindowEvent e) {

    }

    @Override
    public void valueChanged(ListSelectionEvent e) {

    }
}
