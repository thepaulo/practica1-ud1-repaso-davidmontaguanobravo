package davidmontaguano.tiendacocinas.gui;

import davidmontaguano.tiendacocinas.base.CocinaAmericana;
import davidmontaguano.tiendacocinas.base.CocinaIsla;
import davidmontaguano.tiendacocinas.base.TiendaCocinas;

import java.time.LocalDate;
import java.util.ArrayList;

import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.crypto.dsig.TransformException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;

public class CocinaModelo {

    private ArrayList<TiendaCocinas> listaCocinas;
    private int posicion;

    public CocinaModelo(){
        listaCocinas=new ArrayList<TiendaCocinas>();
        posicion=0;
    }

    public ArrayList<TiendaCocinas> obtenerCocinas() {
        return listaCocinas;
    }
    //MEtodo para crear el alta de una cocina americana
    public void altaAmericana(String nombreAccesorio, String marca, String movilidad, int medida, LocalDate fechaCompra, String tamanioBarra){
        CocinaAmericana nuevaAmericana=new CocinaAmericana(nombreAccesorio,marca,movilidad,medida,fechaCompra,tamanioBarra);
        listaCocinas.add(nuevaAmericana);
    }
    //MEtodo para crear el alta de una cocina tipo isla
    public void altaIsla(String nombreAccesorio, String marca, String movilidad, int medida, LocalDate fechaCompra, int nEncimeras){
        CocinaIsla nuevaIsla=new CocinaIsla(nombreAccesorio,marca,movilidad,medida,fechaCompra,nEncimeras);
        listaCocinas.add(nuevaIsla);
    }

    //Metodo para controlar si un articulo ya existe
    public boolean existeAccesorio(String nombreAccesorio){
        for(TiendaCocinas unAccesorio:listaCocinas){
            if(unAccesorio.getNombreAccesorio().equals(nombreAccesorio)){
                return true;
            }
        }
        return false;
    }

    //Metodo que usamos para la exportacion de nuesto documento creado en la ventana.
    //THROWS nos sirve para controlar e identificar posibles execpciones que un posterior bloque de codigo pueda lanzar

    public void exportarXML(File fichero) throws ParserConfigurationException, TransformerException {
        DocumentBuilderFactory factory= DocumentBuilderFactory.newInstance();
        DocumentBuilder builder= factory.newDocumentBuilder();
        DOMImplementation dom=builder.getDOMImplementation();
        Document documento=dom.createDocument(null,"xml",null);


        //Añado el nodo raiz - la primera etiqueta que contiene a las demas

        Element raiz= documento.createElement("Cocinas");
        documento.getDocumentElement().appendChild(raiz);

        Element nodoCocina=null, nodoDatos=null;
        Text texto=null;

        for(TiendaCocinas unAccesorio:listaCocinas){

            /*Añado dentro de la etiqueta raiz (Tienda cocinas) una etiqueta
            dependiendo del tipo de cocina(Americana e Isla) que este almacenando

             */
            if(unAccesorio instanceof CocinaAmericana){
                nodoCocina=documento.createElement("CocinaAmericana");
            }
            else{
                nodoCocina=documento.createElement("CocinaIsla");
            }

            raiz.appendChild(nodoCocina);

            /*Dentro de la etiqueta Cocina le añado
            las subetiquetas con los datos de sus
            atributos (nombreAccesorio, marca, medida, etc)
             */

            nodoDatos=documento.createElement("nombreAccesorio");
            nodoCocina.appendChild(nodoDatos);
            texto=documento.createTextNode(unAccesorio.getNombreAccesorio());
            nodoDatos.appendChild(texto);

            nodoDatos=documento.createElement("marca");
            nodoCocina.appendChild(nodoDatos);
            texto=documento.createTextNode(unAccesorio.getMarca());
            nodoDatos.appendChild(texto);

            nodoDatos=documento.createElement("movilidad");
            nodoCocina.appendChild(nodoDatos);
            texto=documento.createTextNode(unAccesorio.getMovilidad());
            nodoDatos.appendChild(texto);

            nodoDatos=documento.createElement("medida");
            nodoCocina.appendChild(nodoDatos);
            texto=documento.createTextNode(String.valueOf((unAccesorio).getMedida()));
            nodoDatos.appendChild(texto);

            nodoDatos=documento.createElement("fechaCompra");
            nodoCocina.appendChild(nodoDatos);
            texto=documento.createTextNode(unAccesorio.getFechaCompra().toString());
            nodoDatos.appendChild(texto);


             /* Como hay un dato que depende del tipo de Cocina
            debo acceder a él controlando el tipo de objeto
             */
            if(unAccesorio instanceof CocinaAmericana){
                nodoDatos=documento.createElement("tamanioBarra");
                nodoCocina.appendChild(nodoDatos);
                texto=documento.createTextNode(((CocinaAmericana) unAccesorio).getTamanioBarra());
                nodoDatos.appendChild(texto);
            }
            else{
                nodoDatos=documento.createElement("nEncimeras");
                nodoCocina.appendChild(nodoDatos);
                texto=documento.createTextNode(String.valueOf(((CocinaIsla)unAccesorio).getnEncimeras()));
                nodoDatos.appendChild(texto);
            }


        }


        /*
        Guardo los datos en "fichero" que es el objeto File
        recibido por parametro
         */
        Source source = new DOMSource(documento);
        Result resultado = new StreamResult(fichero);

        Transformer transformer = TransformerFactory.newInstance().newTransformer();
        transformer.transform(source,resultado);



    }

    //Metodo para que usaremos para importar archivos xml previamente creados
    public void importarXML(File fichero) throws ParserConfigurationException,IOException,SAXException{
        listaCocinas=new ArrayList<TiendaCocinas>();

        CocinaAmericana nuevoCocinaAmericana=null;
        CocinaIsla nuevoCocinaIsla=null;

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document documento = builder.parse(fichero);

        NodeList listaElementos = documento.getElementsByTagName("*");

        for(int i=0;i<listaElementos.getLength();i++){
            Element nodoAccesorio=(Element) listaElementos.item(i);

            if(nodoAccesorio.getTagName().equals("CocinaAmericana")){
                nuevoCocinaAmericana=new CocinaAmericana();
                nuevoCocinaAmericana.setNombreAccesorio(nodoAccesorio.getChildNodes().item(0).getTextContent());
                nuevoCocinaAmericana.setMarca(nodoAccesorio.getChildNodes().item(1).getTextContent());
                nuevoCocinaAmericana.setMovilidad(nodoAccesorio.getChildNodes().item(2).getTextContent());
                nuevoCocinaAmericana.setMedida(Integer.parseInt(nodoAccesorio.getChildNodes().item(3).getTextContent()));
                nuevoCocinaAmericana.setFechaCompra(LocalDate.parse(nodoAccesorio.getChildNodes().item(4).getTextContent()));
                nuevoCocinaAmericana.setTamanioBarra(nodoAccesorio.getChildNodes().item(5).getTextContent());

                listaCocinas.add(nuevoCocinaAmericana);

            }

            else{
                if(nodoAccesorio.getTagName().equals("CocinaIsla")){
                    nuevoCocinaIsla=new CocinaIsla();
                    nuevoCocinaIsla.setNombreAccesorio(nodoAccesorio.getChildNodes().item(0).getTextContent());
                    nuevoCocinaIsla.setMarca(nodoAccesorio.getChildNodes().item(1).getTextContent());
                    nuevoCocinaIsla.setMovilidad(nodoAccesorio.getChildNodes().item(2).getTextContent());
                    nuevoCocinaIsla.setMedida(Integer.parseInt(nodoAccesorio.getChildNodes().item(3).getTextContent()));
                    nuevoCocinaIsla.setFechaCompra(LocalDate.parse(nodoAccesorio.getChildNodes().item(4).getTextContent()));
                    nuevoCocinaIsla.setnEncimeras(Integer.parseInt(nodoAccesorio.getChildNodes().item(5).getTextContent()));

                    listaCocinas.add(nuevoCocinaIsla);
                }
            }
        }



    }
    //metodo eliminar
    public void eliminar(){
        listaCocinas.remove(posicion);
    }

    public  TiendaCocinas getActual(){
        return listaCocinas.get(posicion);
    }
    //metodo que usaremos para buscar objetos dentro de nuestra tienda
    public TiendaCocinas buscar(String nombreAccesorio){
        for(TiendaCocinas nombre : listaCocinas){
            if(nombre.getNombreAccesorio().equals(nombreAccesorio)){
                return nombre;
            }
        }
        return null;

    }
}
