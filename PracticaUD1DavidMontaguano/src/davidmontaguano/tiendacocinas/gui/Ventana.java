package davidmontaguano.tiendacocinas.gui;

import com.github.lgooddatepicker.components.DatePicker;
import davidmontaguano.tiendacocinas.base.TiendaCocinas;

import javax.swing.*;
import java.awt.*;

//Enumeracion de los elementos de nuestra practica grafica
public class Ventana {

        //Definicion del panel
    private JPanel panel1;
    // Declaracion del JFrame que alverga nuestro elementos
    public JFrame frame;
    //Botones tipo JRadioButton pra elegir entre cocina americana y cocina tipo isla
    public JRadioButton americanaRadioB;
    public JRadioButton islaRadioB;
    //Botnoes tipo JButton para realizar las funciones de Nuevo, Exportar,Importar, Buscar , Eliminar y Salir
    public JButton nuevoBtn;
    public JButton exportarBtn;
    public JButton importarBtn;
    public JButton buscarBtn;
    public JButton salirBtn;
    public JButton eliminarBtn;
    //Recuadros donde introduciremos los datos
    public JTextField accesorioTxt;
    public JTextField marcaTxt;
    public JTextField movilidadTxt;
    public JTextField dimensionTxt;

    //Panel donde se visualizara nuestros datos
    public JList list1;
    //Nombre de los campos a rellenar
    public JLabel Accesorio;
    public JLabel Marca;
    public JLabel Movilidad;
    public JLabel Dimension;
    public JLabel FechaCompra;

    public DatePicker fechaCompraDPicker;

    public JLabel americanaIslaLbl;

    public JTextField americanaIslaTxt;
    public JTextField buscarTxt;

        public DefaultListModel<TiendaCocinas>dlmTiendaCocinas;

    // Metodo donde almacenamos diferentes aspectos de nuestra ventana
    public Ventana(){
        frame = new JFrame("MONBRA TIENDA DE COCINAS");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        frame.setLocationRelativeTo(null);

        initComponents();

    }

    public void initComponents(){
        dlmTiendaCocinas=new DefaultListModel<TiendaCocinas>();
        list1.setModel(dlmTiendaCocinas);
    }
}
