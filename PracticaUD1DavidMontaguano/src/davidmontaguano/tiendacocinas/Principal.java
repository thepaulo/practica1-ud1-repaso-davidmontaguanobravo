package davidmontaguano.tiendacocinas;

import davidmontaguano.tiendacocinas.gui.CocinaControlador;
import davidmontaguano.tiendacocinas.gui.CocinaModelo;
import davidmontaguano.tiendacocinas.gui.Ventana;

public class Principal {

    public static void main(String[] args){
        Ventana vista = new Ventana();
        CocinaModelo modelo= new CocinaModelo();
        CocinaControlador controlador= new CocinaControlador(vista,modelo);
    }


}